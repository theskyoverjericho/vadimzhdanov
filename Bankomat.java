import java.util.Random;
import java.util.Scanner;

public class Bankomat {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int polomka = random.nextInt(10);
        int bogatstvo = random.nextInt(100000);
        int pizdecslomalos = polomka % 3;
        int minus = bogatstvo * -1;
        if (pizdecslomalos == 1) {
            System.out.println("Банкомат неисправен, спасибо что воспользовались нашими услугами");
        } else {
            System.out.println("Вставьте карту");
            System.out.println(" o_o ");
            System.out.println(" O_O ");
            System.out.println(" 0_0 ");
            System.out.println(" Добро пожаловать в систему <<Рваный чирик>> ");
            System.out.println(" Ваш баланс: " + bogatstvo + " рублей");
            System.out.println(" Внесение наличных недоступно");
            System.out.println(" Желаете снять наличные? ");
            if ((bogatstvo == 0) | (bogatstvo <= 10)) {
                System.out.println(" Нищеброд! Уходи! ");
                return;
            }
            System.out.println(" Введите сумму ");
            int value = scanner.nextInt();
            if (value == 0) {
                System.out.println(" Ну не хотите, как хотите. До свидания"); // Пользователь ввёл 0
            } else if ((value > 0) & (value <= bogatstvo)) {

                // Пользователь ввёл нормальную сумму
                System.out.println(" Желаете снять " + value + " рублей?");
                if (value == bogatstvo) {
                    System.out.println(" Это все имеющиеся средства на счету ");
                }
                int a = value / 5000;
                int b = (value - a * 5000) / 2000;
                int c = (value - a * 5000 - b * 2000) / 1000;
                int d = (value - a * 5000 - b * 2000 - c * 1000) / 500;
                int e = (value - a * 5000 - b * 2000 - c * 1000 - d * 500) / 200;
                int f = (value - a * 5000 - b * 2000 - c * 1000 - d * 500 - e * 200) / 100;
                int g = (value - a * 5000 - b * 2000 - c * 1000 - d * 500 - e * 200 - f * 100) / 50;
                int h = (value - a * 5000 - b * 2000 - c * 1000 - d * 500 - e * 200 - f * 100 - g * 50) / 10;
                System.out.println(" Выдано купюр:");
                    if (a>0){System.out.println("5000: " + a);}
                    if (b>0){System.out.println("2000: " + b);}
                    if (c>0){System.out.println("1000: " + c);}
                    if (d>0){System.out.println("500: " + d);}
                    if (e>0){System.out.println("200: " + e);}
                    if (f>0){System.out.println("100: " + f);}
                    if (g>0){System.out.println("50: " + g);}
                    if (h>0){System.out.println("10: " + h);}
                System.out.println(" Всего выдано: " + value / 10 * 10 + "рублей");
            }

            if (value > bogatstvo) { //пользователь ввёл сумму больше имеющихся средств
                System.out.println(" Недостаточно средств на счету. Желаете снять всю имеющуюся сумму? ");
                value = bogatstvo;
                int a1 = value / 5000;
                int b1 = (value - a1 * 5000) / 2000;
                int c1 = (value - a1 * 5000 - b1 * 2000) / 1000;
                int d1 = (value - a1 * 5000 - b1 * 2000 - c1 * 1000) / 500;
                int e1 = (value - a1 * 5000 - b1 * 2000 - c1 * 1000 - d1 * 500) / 200;
                int f1 = (value - a1 * 5000 - b1 * 2000 - c1 * 1000 - d1 * 500 - e1 * 200) / 100;
                int g1 = (value - a1 * 5000 - b1 * 2000 - c1 * 1000 - d1 * 500 - e1 * 200 - f1 * 100) / 50;
                int h1 = (value - a1 * 5000 - b1 * 2000 - c1 * 1000 - d1 * 500 - e1 * 200 - f1 * 100 - g1 * 50) / 10;
                System.out.println(" Выдано купюр:");
                if (a1>0){System.out.println("5000: " + a1);}
                if (b1>0){System.out.println("2000: " + b1);}
                if (c1>0){System.out.println("1000: " + c1);}
                if (d1>0){System.out.println("500: " + d1);}
                if (e1>0){System.out.println("200: " + e1);}
                if (f1>0){System.out.println("100: " + f1);}
                if (g1>0){System.out.println("50: " + g1);}
                if (h1>0){System.out.println("10: " + h1);}
            }
            if ((value < 0) & (value >= minus)) { //пользователь ввёл меньше 0
                value = value * -1;
                System.out.println(" Желаете снять " + value + " рублей?");
                int a2 = value / 5000;
                int b2 = (value - a2 * 5000) / 2000;
                int c2 = (value - a2 * 5000 - b2 * 2000) / 1000;
                int d2 = (value - a2 * 5000 - b2 * 2000 - c2 * 1000) / 500;
                int e2 = (value - a2 * 5000 - b2 * 2000 - c2 * 1000 - d2 * 500) / 200;
                int f2 = (value - a2 * 5000 - b2 * 2000 - c2 * 1000 - d2 * 500 - e2 * 200) / 100;
                int g2 = (value - a2 * 5000 - b2 * 2000 - c2 * 1000 - d2 * 500 - e2 * 200 - f2 * 100) / 50;
                int h2 = (value - a2 * 5000 - b2 * 2000 - c2 * 1000 - d2 * 500 - e2 * 200 - f2 * 100 - g2 * 50) / 10;
                System.out.println(" Выдано купюр:");
                if (a2>0){System.out.println("5000: " + a2);}
                if (b2>0){System.out.println("2000: " + b2);}
                if (c2>0){System.out.println("1000: " + c2);}
                if (d2>0){System.out.println("500: " + d2);}
                if (e2>0){System.out.println("200: " + e2);}
                if (f2>0){System.out.println("100: " + f2);}
                if (g2>0){System.out.println("50: " + g2);}
                if (h2>0){System.out.println("10: " + h2);}
                System.out.println(" Всего выдано: " + value / 10 * 10 + "рублей");
            }
            if (value < minus) {               // пользователь ввёл сумму меньше суммы на счету умноженной на -1
                value = bogatstvo;
                System.out.println(" Недостаточно средств на счету. Желаете снять всю имеющуюся сумму? ");
                int a3 = value / 5000;
                int b3 = (value - a3 * 5000) / 2000;
                int c3 = (value - a3 * 5000 - b3 * 2000) / 1000;
                int d3 = (value - a3 * 5000 - b3 * 2000 - c3 * 1000) / 500;
                int e3 = (value - a3 * 5000 - b3 * 2000 - c3 * 1000 - d3 * 500) / 200;
                int f3 = (value - a3 * 5000 - b3 * 2000 - c3 * 1000 - d3 * 500 - e3 * 200) / 100;
                int g3 = (value - a3 * 5000 - b3 * 2000 - c3 * 1000 - d3 * 500 - e3 * 200 - f3 * 100) / 50;
                int h3 = (value - a3 * 5000 - b3 * 2000 - c3 * 1000 - d3 * 500 - e3 * 200 - f3 * 100 - g3 * 50) / 10;
                if (a3>0){System.out.println("5000: " + a3);}
                if (b3>0){System.out.println("2000: " + b3);}
                if (c3>0){System.out.println("1000: " + c3);}
                if (d3>0){System.out.println("500: " + d3);}
                if (e3>0){System.out.println("200: " + e3);}
                if (f3>0){System.out.println("100: " + f3);}
                if (g3>0){System.out.println("50: " + g3);}
                if (h3>0){System.out.println("10: " + h3);}
            }

        }
    }
}